# ML-CN-KG-4-AKI-prediction
Machine Learning, Clinical Notes and Knowledge Graphs for Early Prediction of Acute Kidney Injury in the Intensive Care
![model](img/model.png "Model architecture")

## Running
Each of the four LSTM notebooks runs the same LSTM model using different data of different type, or different combination of data, as input. The input configuration are:

- only clinical features (`LSTM_features.ipynb`)
- only clinical notes (`LSTM_notes.ipynb`)
- clinical features + clinical notes (`LSTM_feature_notes.ipynb`)
- clinical features + clinical notes [retrofitted](https://arxiv.org/abs/1411.4166) with either UMLS or SNOMED-CT (`LSTM_feature_retrofitted_notes.ipynb`)

### Preliminary steps

Before running the LSTM models some preliminary steps are needs to be performed:

1. Set-up the [MIMIC III](https://mimic.mit.edu/docs/iii/) database 
2. Run the following SQL scripts from the [MIMIC code](https://github.com/MIT-LCP/mimic-code) repository:
	- `mimic-iii/concepts/echo-data.sql`
	- `mimic-iii/concepts/demographics/icustay_detail.sql`
	- `mimic-iii/concepts/durations/weight-durations.sql`
	- `mimic-iii/concepts/durations/vasopressor-durations.sql`
	- `mimic-iii/concepts/durations/ventilation-durations.sql`
	- `mimic-iii/concepts/fluid-balance/urine-output.sql`
	- `mimic-iii/concepts/organfailure/kdigo-creatinine.sql`
	- `mimic-iii/concepts/organfailure/kdigo-stages-48hr.sql`
	- `mimic-iii/concepts/organfailure/kdigo-stages-7day.sql`
	- `mimic-iii/concepts/organfailure/kdigo-stages.sql`
	- `mimic-iii/concepts/organfailure/kdigo-uo.sql`
3. Run the SQL scripts from the repository [Interpretable and continuous AKI prediction in the intensive care](https://github.com/mapo89/continuous-aki-predict) which should generate the following data files:
	- `kdigo_stages_measured.csv` is a time-series of creatinine and urine output measurements and the corresponding and the corresponding AKI labels. Urine output is computed over the last six, 12 and 24 hours.
	- `icustay_detail-kdigo_stages_measured.csv` collects non-temporal variables of patient demographics such as: age (numerical), gender (binary), ethnicity group (categorical) and type of admission (categorical).
	- `labs-kdigo_stages_measured.csv` is a time-series of laboratory tests.
	- `vitals-kdigo_stages_measured.csv` is a time-series of vital signs' measurements.
	- `vents-vasopressor-sedatives-kdigo_stages_measured.csv` hosts temporal information on whether mechanical ventilation, vasopressor or sedative medications were applied.
4. To use [UMLS](https://www.nlm.nih.gov/research/umls/index.html), a [UMLS release](https://www.nlm.nih.gov/research/umls/licensedcontent/umlsknowledgesources.html) is needed
	
### Using clinical features and clinical notes

In order to use clinical features, convert the CSV file data from point 3. into the [MIMIC-III Benchmarks](https://github.com/YerevaNN/mimic3-benchmarks) format running the `preprocessing.ipynb` notebook. Further code from the MIMIC-III Benchmarks repository is reused and adapted to read the data in `mimic_utils_text.ipynb`.

To use clinical notes, extract and preprocessed clinical notes with the `extract_text.ipynb` notebook.  The output is a `dump_notes.txt` file which can be used as input of the `demo.sh` script in the [Glove repository](https://github.com/stanfordnlp/GloVe) to learn word embeddings. The outputs are

 - a text file with the learned word vectors, e.g. vectors.txt;
 - a vocabulary text file with the unique tokens and their frequencies in the corpus, e.g. vocab.txt.

Clincal notes can be retrofitted with the `retrofit.ipynb` notebook, which reuses the [retrofitting method](https://github.com/mfaruqui/retrofitting). Such script needs a lexicon and word embeddings (generated with GloVe) as input. The lexicon file can be generated first running

```
python knowledge_graphs/extract-umls.py
```

if using UMLS, and the running

```
python knowledge_graphs/create_lexicon.py
```

## Dependencies
- Jupyter notebook
- torch
- numpy
- pandas
- scipy
- sklearn
- re
- seaborn

## Acknowledgments

We thank [Miguel A. Rios Ganoa](https://github.com/mriosb08) for the help to extract the clinical notes.

The `preprocessing.ipynb` script reuses and adapts code from the [MIMIC-III Benchmarks](https://github.com/YerevaNN/mimic3-benchmarks) repository to convert the MIMIC III extracted data to the proper format. Most notably we use a different set of clinical features as input. The `extract_text.ipynb` notebook extends the MIMIC-III Benchmarks to be used not only with clinical features, but also with text.

## Cite

If you use our code in your own work please cite our paper (currenlty in press):

    @inproceedings{Vagliano:2021,
         author = {Vagliano, Iacopo and Hsu, Wei-Hsiang and Schut, Martijn C},
         title = {Machine Learning, Clinical Notes and Knowledge Graphs for Early Prediction of Acute Kidney Injury in the Intensive Care},
         series = {19th International Conference on Informatics, Management and Technology in Healthcare (ICIMTH 2021)}
	     year = {In press},
    }
