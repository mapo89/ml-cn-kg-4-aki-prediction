from owlready2 import *
from owlready2.pymedtermino2 import *
from owlready2.pymedtermino2.umls import *

UMLS_PATH = "../../../umls-2020AB-full.zip"

default_world.set_backend(filename = "pym.sqlite3")
import_umls(UMLS_PATH, terminologies = ["ICD10", "SNOMEDCT_US", "CUI"])
default_world.save()
