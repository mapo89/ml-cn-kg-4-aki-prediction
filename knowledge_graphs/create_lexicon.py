from owlready2 import *

default_world.set_backend(filename = "pym.sqlite3")
PYM = get_ontology("http://PYM/").load()
# Use UMLS
CUI = PYM["CUI"]
# Use SNOMED-CT
SNOMEDCT_US = PYM["SNOMEDCT_US"]
# Choose either 'UMLS' or 'SNOMED-CT'
ONTOLOGY = 'SNOMED-CT'
# Use either 'synonyms' or 'children'
# PROPERTY = 'synonyms'
PROPERTY = 'children'

#concept = CUI["C0085580"]
#print(concept.label)  # ['Essential hypertension']
#print(concept.synonyms)  # ['Essential (primary) hypertension', 'Idiopathic hypertension', 'Primary hypertension', 'Systemic primary arterial hypertension', 'Essential hypertension (disorder)']
#print("{} {}".format(concept.label[0],' '.join([s.replace(' ', '_') for s in concept.synonyms])))
# FIXME do we have hierarchy info in UMLS? How to use it if yes? Why children and descendant_concepts return same # of concepts?
#concept = SNOMEDCT_US["80891009"] # Heart structure
#print(concept)
#print(concept.get_class_properties())
#print(CUI.get_class_properties())
#print(len(CUI.children))
#print(len(CUI.descendant_concepts()))
#print(concept.label)  # ['Heart structure']
#print(concept.children)
#print(concept.parents)
#for label in concept.label:
#    children = ""
#    for c in concept.children:
#        children += ' '.join([s.replace(' ', '_') for s in c.label])
#        children += ' '
#    print("{} {}".format(label.replace(' ', '_'), children))
print("Reading and writing {} from {}...".format("synonyms" if PROPERTY == 'synonyms' else "children", ONTOLOGY))
with open('lexicon.txt', 'w') as f:
    root = CUI if ONTOLOGY == "UMLS" else SNOMEDCT_US
    for concept in root.descendant_concepts():
        if PROPERTY == 'synonyms':
            # Create lexicon with synonyms
            label = concept.label[0].replace(' ', '_') if len(concept.label) == 1 else ' '.join([l.replace(' ', '_') for l in concept.label])
            print("{} {}".format(label,' '.join([s.replace(' ', '_') for s in concept.synonyms])), file=f)
        else:
            # Create lexicon with children
            # if the concept has multiple label add children for each label
            for label in concept.label:
                children = ""
                for c in concept.children:
                    children += ' '.join([s.replace(' ', '_') for s in c.label])
                    children += ' '
                print("{} {}".format(label.replace(' ', '_'), children), file=f)
